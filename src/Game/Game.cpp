#include "Game.h"
#include <iostream>
#include <SDL_image.h>
#include <glm\glm.hpp>
#include <fstream>

#include "..\Logger\Logger.h"
#include "..\Components\TransformComponent.h"
#include "..\Components\RigidBodyComponent.h"
#include "..\Components\SpriteComponent.h"
#include "..\Components\AnimationComponent.h"
#include "../Components/BoxColliderComponent.h"
#include "..\Systems\MovementSystem.h"
#include "..\Systems\RenderSystem.h"
#include "../Systems/AnimationSystem.h"
#include "../Systems/CollisionSystem.h"

#include "..\TileMapper.h"


#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720

Game::Game() {
	registry = std::make_unique<Registry>();
	assetManager = std::make_unique<AssetManager>();
	Logger::Log("Game Constructor Called!");
}

Game::~Game() {
	Logger::Error("Game Destructor Called!");
}

void Game::Initialize() {

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		Logger::Error("Failed to Initialize SDL");
		return;
	}

	window = SDL_CreateWindow(
		NULL,
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		WINDOW_WIDTH,
		WINDOW_HEIGHT,
		SDL_WINDOW_BORDERLESS
	);

	if (window == NULL) {
		Logger::Error("Failed to create SDL window");
		return;
	}

	renderer = SDL_CreateRenderer(
		window,
		-1,
		0
	);

	if (renderer == NULL) {
		Logger::Error("Failed to create SDL Renderer");
		return;
	}

	IsRunning = true;
}

void Game::Setup() {
	LoadLevel(1);
}

void Game::LoadLevel(int levelNumber) {
	registry->AddSystem<MovementSystem>();
	registry->AddSystem<RenderSystem>();
	registry->AddSystem<AnimationSystem>();
	registry->AddSystem<CollisionSystem>();

	assetManager->AddTexture(renderer, std::string("tank-right"), std::string("./assets/images/tank-panther-right.png"));
	assetManager->AddTexture(renderer, std::string("tank-left"), std::string("./assets/images/landing-base.png"));
	assetManager->AddTexture(renderer, std::string("chopper-image"), std::string("./assets/images/chopper.png"));
	assetManager->AddTexture(renderer, std::string("radar-image"), std::string("./assets/images/radar.png"));

	TileMapper::GetInstance()->LoadTileMap(1, assetManager, registry, renderer);

	Entity chopper = registry->CreateEntity();
	chopper.AddComponent<TransformComponent>(glm::vec2(100.0, 10.0), glm::vec2(1.0, 1.0), 0.0);
	chopper.AddComponent<SpriteComponent>("chopper-image", 32, 32, 1);
	chopper.AddComponent<AnimationComponent>(2, 15, true);

	Entity radar = registry->CreateEntity();
	radar.AddComponent<TransformComponent>(glm::vec2(WINDOW_WIDTH - 74, 10.0), glm::vec2(1.0, 1.0), 0.0);
	radar.AddComponent<SpriteComponent>("radar-image", 64, 64, 1);
	radar.AddComponent<AnimationComponent>(8, 5, true);

	Entity tank = registry->CreateEntity();
	tank.AddComponent<TransformComponent>(glm::vec2(10.0, 30.0), glm::vec2(1.0, 1.0), 0.0);
	tank.AddComponent<RigidBodyComponent>(glm::vec2(50.0, 0.0));
	tank.AddComponent<SpriteComponent>("tank-left", 32, 32, 2);
	tank.AddComponent<BoxColliderComponent>(32, 32);

	Entity enemy = registry->CreateEntity();
	enemy.AddComponent<TransformComponent>(glm::vec2(1000.0, 30.0), glm::vec2(1.0, 1.0), 0.0);
	enemy.AddComponent<RigidBodyComponent>(glm::vec2(-50.0, 0.0));
	enemy.AddComponent<SpriteComponent>("tank-right", 32, 32, 1);
	enemy.AddComponent<BoxColliderComponent>(32, 32);
}

void Game::Run() {
	Setup();
	while (IsRunning) {
		ProcessInput();
		Update();
		Render();
	}
	Destroy();
}

void Game::ProcessInput() {

	SDL_Event sdlEvent;
	while (SDL_PollEvent(&sdlEvent))
	{
		switch (sdlEvent.type)
		{
		case SDL_QUIT:
			IsRunning = false;
			break;

		case SDL_KEYDOWN:
			if (sdlEvent.key.keysym.sym == SDLK_ESCAPE) {
				IsRunning = false;
			}
			break;

		default:
			break;
		}
	}
}

void Game::Update() {

	int timeToWait = MS_PER_FRAME - (SDL_GetTicks() - previousFrameTime);

	if (timeToWait > 0) {
		SDL_Delay(timeToWait);
	}

	double deltaTime = (SDL_GetTicks() - previousFrameTime) / 1000.0;
	previousFrameTime = SDL_GetTicks();

	registry->Update();
	registry->GetSystem<MovementSystem>().Update(deltaTime);
	registry->GetSystem<AnimationSystem>().Update();
	registry->GetSystem<CollisionSystem>().Update();
}

void Game::Render() {
	SDL_SetRenderDrawColor(renderer, 21, 21, 21, 255);
	SDL_RenderClear(renderer);
	registry->GetSystem<RenderSystem>().Update(renderer, assetManager);
	SDL_RenderPresent(renderer);
}

void Game::Destroy() {
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}
