#ifndef GAME_H
#define GAME_H

#include <SDL.h>
#include "../ECS/ECS.h"
#include "../AssetManager/AssetManager.h"

const int FPS = 60;
const int MS_PER_FRAME = (1000 / FPS);

class Game {

private:
	SDL_Window *window;
	SDL_Renderer *renderer;
	bool IsRunning;
	int previousFrameTime;
	std::unique_ptr<Registry> registry;
	std::unique_ptr<AssetManager> assetManager;
public:
	Game();
	~Game();
	void Initialize();
	void Run();
	void Setup();
	void LoadLevel(int levelNumber);
	void ProcessInput();
	void Update();
	void Render();
	void Destroy();
};
#endif
