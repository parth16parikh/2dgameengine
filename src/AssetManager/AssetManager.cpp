#include "AssetManager.h"
#include "../Logger/Logger.h"
#include <SDL_image.h>

AssetManager::AssetManager() {
	Logger::Log("Assetmanager created");
}

AssetManager::~AssetManager() {
	ClearTextures();
	Logger::Log("Assetmanager destroyed");
}

void AssetManager::ClearTextures() {
	for (auto texture : textures) {
		SDL_DestroyTexture(texture.second);
	}
	textures.clear();
}

void AssetManager::AddTexture(SDL_Renderer* renderer, std::string& assetId, std::string& filePath) {
	SDL_Surface* surface = IMG_Load(filePath.c_str());
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);

	textures.emplace(assetId, texture);

	Logger::Log("New texture added with id "+ assetId);
}

SDL_Texture* AssetManager::GetTexture(const std::string& assetID) const {
	return textures.at(assetID);
}