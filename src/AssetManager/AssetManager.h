#ifndef ASSET_MANAGER_H
#define ASSET_MANAGER_H

#include <map>
#include <string>
#include <SDL.h>

class AssetManager {
private:
	std::map<std::string, SDL_Texture*> textures;

public:
	AssetManager();
	~AssetManager();
	void ClearTextures();
	void AddTexture(SDL_Renderer* renderer, std::string& assetID, std::string& filePath);
	SDL_Texture* GetTexture(const std::string& assetID) const;
};

#endif // !ASSET_MANAGER_H

