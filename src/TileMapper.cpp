#include "TileMapper.h"
#include <fstream>
#include "Components\TransformComponent.h"
#include "Components\SpriteComponent.h"

std::unique_ptr<TileMapper> TileMapper::m_Instance = 0;

std::unique_ptr<TileMapper>& TileMapper::GetInstance() {
	if (!m_Instance) {
		m_Instance = std::make_unique<TileMapper>();
	}
	return m_Instance;
}

void TileMapper::LoadTileMap(int levelNumber, std::unique_ptr<AssetManager>& assetManager, std::unique_ptr<Registry>& registry, SDL_Renderer* renderer) {

	std::string assetId;
	std::string textureFilePath;
	int tileWidth;
	int tileheight;
	double tileScale;
	int mapNumOfRows;
	int mapNumOfCols;
	std::string tileMapFilePath;

	if (levelNumber == 1) {
		assetId = "jungle-tilemap";
		textureFilePath = "./assets/tilemaps/jungle.png";
		tileWidth = 32;
		tileheight = 32;
		tileScale = 2.0f;
		mapNumOfRows = 20;
		mapNumOfCols = 25;
		tileMapFilePath = "./assets/tilemaps/jungle.map";
	}

	assetManager->AddTexture(renderer, std::string(assetId), std::string(textureFilePath));
	std::fstream mapFile;
	mapFile.open(tileMapFilePath);

	for (int y = 0; y < mapNumOfRows; y++) {
		for (int x = 0; x < mapNumOfCols; x++) {
			char ch;
			mapFile.get(ch);
			int srcRectY = std::atoi(&ch) * tileheight;
			mapFile.get(ch);
			int srcRectX = std::atoi(&ch) * tileWidth;
			mapFile.ignore();

			Entity tile = registry->CreateEntity();
			tile.AddComponent<TransformComponent>(glm::vec2(x * tileWidth * tileScale, y * tileheight * tileScale), glm::vec2(tileScale, tileScale));
			tile.AddComponent<SpriteComponent>(assetId, tileWidth, tileheight,0, srcRectX, srcRectY);
		}
	}
	mapFile.close();
}
