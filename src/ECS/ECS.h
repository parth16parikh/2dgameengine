#ifndef ECS_H
#define ECS_H

#include <bitset>
#include <vector>
#include <unordered_map>
#include <typeindex>
#include <set>
#include "../Logger/Logger.h"

const unsigned int MAX_COMPONENTS = 32;
typedef std::bitset<MAX_COMPONENTS> Signature;

struct IComponent {
protected:
	static int nextId;
};

template <typename T>
class Component : public IComponent {
public:
	static int GetId() {
		static auto Id = nextId++;
		return Id;
	}
};

class Entity {
private:
	int id;

public:
	Entity() = default;
	Entity(int id) : id(id) { };
	Entity(const Entity& other) = default;
	int GetId() const;
	
	Entity& operator =(const Entity& other) = default;
	bool operator == (const Entity& other) const {
		return GetId() == other.GetId();
	}
	bool operator != (const Entity& other) const {
		return GetId() != other.GetId();
	}
	bool operator < (const Entity& other) const {
		return GetId() < other.GetId();
	}

	template <typename T, typename ...TArgs> void AddComponent(TArgs&& ...args);
	template <typename T> T& GetComponent();
	template <typename T> void RemoveComponent();
	template <typename T> bool HasComponent() const;

	class Registry *registry;
};

template <typename T, typename ...TArgs> 
void Entity::AddComponent(TArgs&& ...args) 
{
	registry->AddComponent<T>(*this, std::forward<TArgs>(args)...);
}

template <typename T> 
T& Entity::GetComponent()
{
	return registry->GetComponent<T>(*this);
}

template <typename T> 
void Entity::RemoveComponent()
{
	registry->RemoveComponent<T>(*this);
}

template <typename T> 
bool Entity::HasComponent() const
{
	return registry->HasComponent<T>(*this);
}


class System {
private:
	Signature componentSignature;
	std::vector<Entity> entities;
public:
	System() = default;
	~System() = default;
	void AddEntity(Entity entity);
	void RemoveEntity(Entity entity);
	std::vector<Entity> GetSystemEntities() const;
	const Signature& GetComponentSignature() const;

	template <typename T> void RequireComponent();
};


class IPool {
public:
	virtual ~IPool() {}
};

template <typename T>
class Pool : public IPool{
private:
	std::vector<T> data;

public:
	Pool(int size = 100) {
		data.resize(size);
	}

	virtual ~Pool() = default;

	bool IsEmpty() const{
		return data.empty();
	}

	int GetSize() const {
		return data.size();
	}

	void Resize(int size) {
		data.resize(size);
	}

	void Clear() {
		data.clear();
	}

	void Add(T object) {
		data.push_back(object);
	}

	void Set(unsigned int index, T object) {
		data[index] = object;
	}

	T& Get(unsigned int index) {
		return static_cast<T&>(data[index]);
	}

	T& operator [] (unsigned int index) {
		return data[index];
	}
};

class Registry {
private:
	int numEntity = 0;
	std::vector<std::shared_ptr<IPool>> componentPools;
	std::vector<Signature> entityComponentSignatures;
	std::unordered_map<std::type_index, std::shared_ptr<System>> systems;
	std::set<Entity> entitiesToBeAdded;
	std::set<Entity> entitiesToBeKilled;
public:
	Registry() {
		Logger::Log("Registry constructor called");
	}

	~Registry() {
		Logger::Log("Registry destructor called");
	}

	Entity CreateEntity();

	template <typename T, typename ...TArgs> void AddComponent(Entity entity, TArgs&& ...args);
	template <typename T> T& GetComponent(Entity entity) const;
	template <typename T> void RemoveComponent(Entity entity);
	template <typename T> bool HasComponent(Entity entity) const;

	template <typename T, typename ...TArgs> void AddSystem(TArgs&& ...args);
	template <typename T> void RemoveSystem();
	template <typename T> bool HasSystem() const;
	template <typename T> T& GetSystem() const;

	void AddEntityToSystem(Entity entity);

	void Update();
};

template <typename T, typename ...TArgs>
void Registry::AddComponent(Entity entity, TArgs&& ...args) {
	const unsigned int componentId = Component<T>::GetId();
	const unsigned int entityId = entity.GetId();

	if (componentId >= componentPools.size()) {
		componentPools.resize(componentId + 1, nullptr);
	}

	if (!componentPools[componentId]) {
		std::shared_ptr<Pool<T>> newComponentPool(new Pool<T>());
		componentPools[componentId] = newComponentPool;
	}
	std::shared_ptr<Pool<T>> componentPool = std::static_pointer_cast<Pool<T>>(componentPools[componentId]);

	if (entityId >= componentPool->GetSize()) {
		componentPool->Resize(numEntity);
	}

	T newComponent(std::forward<TArgs>(args)...);
	componentPool->Set(entityId, newComponent);
	entityComponentSignatures[entityId].set(componentId);
}

template <typename T> 
T& Registry::GetComponent(Entity entity) const
{
	const auto componentId = Component<T>::GetId();
	const auto entityId = entity.GetId();
	auto componentPool = std::static_pointer_cast<Pool<T>>(componentPools[componentId]);
	return componentPool->Get(entityId);
}

template <typename T>
void Registry::RemoveComponent(Entity entity) {
	const int componentId = Component<T>::GetId();
	const int entityId = entity.GetId();

	entityComponentSignatures[entityId].set(componentId, false);
}

template <typename T>
bool Registry::HasComponent(Entity entity) const {
	const int componentId = Component<T>::GetId();
	const int entityId = entity.GetId();

	return entityComponentSignatures[entityId].test(componentId);
}

template <typename T, typename ...TArgs>
void Registry::AddSystem(TArgs&& ...args) {
	std::shared_ptr<T> newSystem = std::make_shared<T>(std::forward<TArgs>(args)...);
	systems.insert(std::make_pair(std::type_index(typeid(T)), newSystem));
}

template <typename T>
void Registry::RemoveSystem() {
	auto system = systems.find(std::type_index(typeid(T)));
	systems.erase(system);
}

template <typename T>
bool Registry::HasSystem() const {
	return systems.find(std::type_index(typeid(T))) != systems.end();
}

template <typename T>
T& Registry::GetSystem() const {
	auto system = systems.find(std::type_index(typeid(T)));
	return *(std::static_pointer_cast<T>(system->second));
}

template <typename T>
void System::RequireComponent() {
	int id = Component<T>::GetId();
	componentSignature.set(id);
}

#endif // !ECS_H