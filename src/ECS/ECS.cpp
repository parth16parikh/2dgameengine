#include "ECS.h"
#include <algorithm>
#include "../Logger/Logger.h"

int IComponent::nextId = 0;

int Entity::GetId() const {
	return id;
}

void System::AddEntity(Entity entity) {
	entities.push_back(entity);
}

void System::RemoveEntity(Entity entity) {
	entities.erase(std::remove_if(entities.begin(), entities.end(), [&entity](Entity other) {
		return entity == other;
	}), entities.end());
}

std::vector<Entity> System::GetSystemEntities() const {
	return entities;
}

const Signature& System::GetComponentSignature() const {
	return componentSignature;
}

Entity Registry::CreateEntity() {
	int entityId = numEntity++;
	Entity entity(entityId);
	entity.registry = this;
	entitiesToBeAdded.insert(entity);
	if (entityId >= entityComponentSignatures.size()) {
		entityComponentSignatures.resize(entityId + 1);
	}
	Logger::Log("Entity Created with Id : " + std::to_string(entityId));
	return entity;
}

void Registry::Update() {

	for (auto entity : entitiesToBeAdded)
	{
		AddEntityToSystem(entity);
	}
	entitiesToBeAdded.clear();
}

void Registry::AddEntityToSystem(Entity entity) {
	const int entityId = entity.GetId();

	Signature& entityComponentSignature = entityComponentSignatures[entityId];

	for (auto system : systems) {
		const Signature& systemComponentSignature = system.second->GetComponentSignature();

		bool isInterested = ((entityComponentSignature & systemComponentSignature) == systemComponentSignature);

		if (isInterested) {
			system.second->AddEntity(entity);
		}
	}
}