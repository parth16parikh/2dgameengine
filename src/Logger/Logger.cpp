#include "Logger.h"
#include <iostream>
#include <chrono>
#include <ctime>
#include <string>

#define NC "\033[0m"
#define GREEN "\x1B[32m"
#define RED "\x1B[91m"

std::vector<LogEntry> Logger::m_Messages;

void Logger::Log(const std::string& message) {
	LogEntry logEntry;
	logEntry.type = INFO;
	logEntry.message = "LOG: " + GetCurrentTime() + ": " + message;
	std::cout << logEntry.message << std::endl;
	m_Messages.push_back(logEntry);
}

void Logger::Error(const std::string& message) {
	LogEntry logEntry;
	logEntry.type = ERROR;
	logEntry.message = "ERROR: " + GetCurrentTime() + ": " + message;
	std::cerr << logEntry.message << std::endl;
	m_Messages.push_back(logEntry);
}

std::string Logger::GetCurrentTime() {
	std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::string currentTime(22, NULL);
	struct tm buf;
	localtime_s(&buf, &now);
	strftime(&currentTime[1], currentTime.size(), "%d-%b-%Y %I:%M:%S", &buf);
	currentTime[0] = '[';
	currentTime[21] = ']';

	return currentTime;
}