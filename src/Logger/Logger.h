#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <vector>

enum LogType {
	INFO,
	WARNING,
	ERROR
};

struct LogEntry {
	LogType type;
	std::string message;
};

class Logger {

private:
	static std::string GetCurrentTime();
public:
	static std::vector<LogEntry> m_Messages;
	static void Log(const std::string& message);
	static void Error(const std::string& message);
};

#endif