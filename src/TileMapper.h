#ifndef TILEMAPPER_H
#define TILEMAPPER_H

#include <memory>
#include "AssetManager\AssetManager.h"
#include "ECS\ECS.h"
#include <SDL.h>

class TileMapper {

private:
	static std::unique_ptr<TileMapper> m_Instance;

public:
	static std::unique_ptr<TileMapper>& GetInstance();

	void LoadTileMap(int LevelNumber, std::unique_ptr<AssetManager>& assetManager, std::unique_ptr<Registry>& registry, SDL_Renderer* renderer);
};
#endif // !TILEMAPPER_H

