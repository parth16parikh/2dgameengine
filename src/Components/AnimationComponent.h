#ifndef ANIMATION_H
#define ANIMATION_H

#include <SDL.h>
struct AnimationComponent {
	int numberOfFrames;
	int currentFrame;
	int frameRate;
	int startTime;
	bool isLoop;

	AnimationComponent(int _numberOfFrames = 0, int _frameRate = 0, bool _isLoop = true) : numberOfFrames(_numberOfFrames), frameRate(_frameRate), isLoop(_isLoop)
	{
		currentFrame = 1;
		startTime = SDL_GetTicks();
	}
};


#endif