#ifndef SPRITERENDERER_H
#define SPRITERENDERER_H

#include <string>
#include <SDL.h>

struct SpriteComponent {
	std::string assetId;
	int width;
	int height;
	SDL_Rect sourceRect;
	int zOrder;

	SpriteComponent(std::string id = "",int _width = 0, int _height = 0, int _zOrder = 0, int srcX = 0, int srcY = 0) : assetId(id), width(_width), height(_height), zOrder(_zOrder) {
	
		sourceRect = {
			srcX,
			srcY,
			width,
			height
		};
	}
};


#endif