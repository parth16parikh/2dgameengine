#ifndef BOXCOLLIDER_H
#define BOXCOLLIDER_H

#include <glm\glm.hpp>

struct BoxColliderComponent {
	int width;
	int height;
	glm::vec2 offset;

	BoxColliderComponent(int _width = 0, int _height = 0, glm::vec2 _offset = glm::vec2(0,0)) : width(_width), height(_height), offset(_offset) {
		
	}
};


#endif