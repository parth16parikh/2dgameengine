#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <glm\glm.hpp>
struct TransformComponent {
	glm::vec2 position;
	glm::vec2 scale;
	double rotation;

	TransformComponent(glm::vec2 pos = glm::vec2(0.0, 0.0), glm::vec2 s = glm::vec2(1.0, 1.0), double rot = 0.0): position(pos), scale(s), rotation(rot) { }
};


#endif