#ifndef ANIMATIONSYSTEM_H
#define ANIMATIONSYSTEM_H

#include <SDL.h>
#include "../ECS/ECS.h"
#include "../Components/AnimationComponent.h"
#include "../Components/SpriteComponent.h"

class AnimationSystem : public System{

public:

	AnimationSystem() {
		RequireComponent<AnimationComponent>();
		RequireComponent<SpriteComponent>();
	}

	void Update() {

		for (auto entity : GetSystemEntities()) {
			SpriteComponent& sprite = entity.GetComponent<SpriteComponent>();
			AnimationComponent& animation = entity.GetComponent<AnimationComponent>();

			animation.currentFrame = ((SDL_GetTicks() - animation.startTime) * animation.frameRate / 1000) % animation.numberOfFrames;
			sprite.sourceRect.x = animation.currentFrame * sprite.width;
		}
	}
};

#endif // !ANIMATIONSYSTEM_H

