
#include "MovementSystem.h"
#include "../Components/TransformComponent.h"
#include "../Components/RigidBodyComponent.h"
#include "../Logger/Logger.h"

MovementSystem::MovementSystem() {
	RequireComponent<TransformComponent>();
	RequireComponent<RigidBodyComponent>();
}

void MovementSystem::Update(const double& deltaTime) {
	for (auto entity : GetSystemEntities()) {
		TransformComponent& transform = entity.GetComponent<TransformComponent>();
		const RigidBodyComponent& rigidBody = entity.GetComponent<RigidBodyComponent>();

		transform.position.x += rigidBody.velocity.x * deltaTime;
		transform.position.y += rigidBody.velocity.y * deltaTime;
	}
}