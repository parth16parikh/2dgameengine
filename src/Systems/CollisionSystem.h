#ifndef COLLISIONSYSTEM_H
#define COLLISIONSYSTEM_H

#include <SDL.h>
#include "../ECS/ECS.h"
#include "../Components/TransformComponent.h"
#include "../Components/BoxColliderComponent.h"

class CollisionSystem : public System {

public:

	CollisionSystem() {
		RequireComponent<TransformComponent>();
		RequireComponent<BoxColliderComponent>();
	}

	void Update() {
		auto entities = GetSystemEntities();
		for (auto i = entities.begin(); i != entities.end(); i++) {
			Entity a = *i;
			TransformComponent& aTransform = a.GetComponent<TransformComponent>();
			BoxColliderComponent& aCollider = a.GetComponent<BoxColliderComponent>();
			for (auto j = i+1; j != entities.end(); j++) {
				Entity b = *j;

				TransformComponent& bTransform = b.GetComponent<TransformComponent>();
				BoxColliderComponent& bCollider = b.GetComponent<BoxColliderComponent>();

				bool collisionHappened =CheckAABBCollision(
					aTransform.position.x + aCollider.offset.x,
					aTransform.position.y + aCollider.offset.y,
					aCollider.width,
					aCollider.height,
					bTransform.position.x + bCollider.offset.x,
					bTransform.position.y + bCollider.offset.y,
					bCollider.width,
					bCollider.height
				);

				if (collisionHappened) {
					Logger::Log("Entity :: " + std::to_string(a.GetId()) + " collided with " + " Entity :: " + std::to_string(b.GetId()));
				}
			}
		}
	}

	bool CheckAABBCollision(double aX, double aY, double aW, double aH, double bX, double bY, double bW, double bH)
	{
		 return aX < bX + bW && aX + aW > bX && aY < bY + bH && aY + aH > bY;
	}
};

#endif // !ANIMATIONSYSTEM_H

