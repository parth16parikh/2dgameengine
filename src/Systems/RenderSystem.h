#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H

#include "../ECS/ECS.h"
#include "../Components/SpriteComponent.h"
#include "../Components/TransformComponent.h"
#include <SDL.h>
#include "../AssetManager/AssetManager.h"
#include <algorithm>

class RenderSystem : public System {

public:
	RenderSystem() {
		RequireComponent<TransformComponent>();
		RequireComponent<SpriteComponent>();
	}

	void Update(SDL_Renderer* renderer, std::unique_ptr<AssetManager>& assetManager) {

		auto entities = GetSystemEntities();

		std::sort(entities.begin(), entities.end(), [](Entity& a, Entity& b) {
			int a_zOrder = a.GetComponent<SpriteComponent>().zOrder;
			int b_zOrder = b.GetComponent<SpriteComponent>().zOrder;
			return a_zOrder < b_zOrder;
		});

		for (auto entity : entities) {
			const TransformComponent& transform = entity.GetComponent<TransformComponent>();
			const SpriteComponent& sprite = entity.GetComponent<SpriteComponent>();

			const SDL_Rect sourceRectangle = sprite.sourceRect;

			const SDL_Rect destinatioRectanle = {
				static_cast<int>(transform.position.x),
				static_cast<int>(transform.position.y),
				static_cast<int>(sprite.width * transform.scale.x),
				static_cast<int>(sprite.height * transform.scale.y)
			};

			SDL_RenderCopyEx(
				renderer,
				assetManager->GetTexture(sprite.assetId),
				&sourceRectangle,
				&destinatioRectanle,
				transform.rotation,
				NULL,
				SDL_FLIP_NONE
			);
		}
	}
};

#endif // !RENDERSYSTEM_H

