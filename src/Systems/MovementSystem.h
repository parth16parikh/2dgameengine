#ifndef MOVEMENTSYSTEM_H
#define MOVEMENTSYSTEM_H

#include "../ECS/ECS.h"
class MovementSystem : public System {

public:
	MovementSystem();
	void Update(const double& deltaTime);
};

#endif // MOVEMENTSYSTEM_H

